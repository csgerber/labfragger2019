package edu.uchicago.gerber.labfragger2019;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements Button2Fragment.OnFragmentInteractionListener {

    private Button btnActivity;
    private boolean whichFrag = false;
   // private FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        btnActivity = findViewById(R.id.btnActivity);
//        btnActivity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                DemoFragment fragmentDemo = (DemoFragment)
//                        getSupportFragmentManager().findFragmentByTag("DEMO");
//
//                fragmentDemo.generateColor();
//
//            }
//        });

      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.replace(R.id.frameContainer, DemoFragment.newInstance("#0000FF"), "DEMO");
      ft.commit();


       // toggleFrag();


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction() {
                        DemoFragment fragmentDemo = (DemoFragment)
                        getSupportFragmentManager().findFragmentByTag("DEMO");

                fragmentDemo.generateColor();

    }
}
