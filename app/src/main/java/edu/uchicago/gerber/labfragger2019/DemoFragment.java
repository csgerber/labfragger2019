package edu.uchicago.gerber.labfragger2019;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DemoFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private FrameLayout frameColor;



    private String mParam1;


    public DemoFragment() {
        // Required empty public constructor
    }



    public static DemoFragment newInstance(String param1) {
        DemoFragment fragment = new DemoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_demo, container, false);

        frameColor = view.findViewById(R.id.frameColor);
        String strHex =  getArguments().getString(ARG_PARAM1);
        frameColor.setBackgroundColor(Color.parseColor(strHex));

        return  view;

    }

    public void generateColor(){

        //start with an array of chars 0 -F
        char[] chars = new char[] {'0', '1', '2', '3','4','5','6','7','8','9',
                                       'A','B','C','D','E','F'};

        //create a Random object
        Random random = new Random();
        //get the len of the array
        int nLen = chars.length;
        //stringbuilder add a "#"
        StringBuilder sb = new StringBuilder();
        sb.append("#");
        //for i 0 - 6
        for (int nC = 0; nC < 6; nC++) {
            sb.append(chars[random.nextInt(nLen )]);
        }
            //randomly assign a hex char

        //return the sb.toString()
        frameColor.setBackgroundColor(Color.parseColor(sb.toString()));


    }

}
